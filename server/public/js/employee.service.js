(function(global){
    var employeeService = {};
    employeeService = function($q,$http){
        var url1 = "/employees";
        var url2 = "/addEmployee";
        var url3 = "/deleteEmployee";
        var url4 = "/employee";
        return {
            getAllEmployees: function(){
                var defer = $q.defer();
                $http.get(url1).then(function(res){
                    defer.resolve(res.data)
                });
                return defer.promise;
            },
            getAllEmployees: function(name){
                var defer = $q.defer();
                $http.get(url4+"/"+name).then(function(res){
                    defer.resolve(res.data);
                });
                return defer.promise;
            },
            postEmployees: function(employee){
                var defer = $q.defer();
                $http.post(url2,employee).then(function(req,res){
                    defer.resolve("Add the employee succesfully")
                });
                return defer.promise;
            },

            deleteEmployee: function(name){
                $http.delete("/deleteEmployee/"+name).then(function(req,res){
                });
            },

            editEmployees: function(employee){
                var defer =$q.defer();
                $http.put("/employee",employee).then(function(res){
                    defer.resolve(res.data)
                });
                return defer.promise;
            }
        };
    };

    global.employeeService = employeeService;
})(window);