var app = angular.module("mainApp", ["ngRoute","ngResource"]);
app.controller("mainCtrl", ["$scope",function($scope){

}]);

app.controller("homeCtrl",["$scope","employeeService","$location","$rootScope",function($scope,employeeService,$location,$rootScope){
    $scope.employees = [];
    employeeService.getAllEmployees().then(function(res){
        $scope.employees = res;
    });
    $scope.doDelete = function(name){
        employeeService.deleteEmployee(name);
        employeeService.getAllEmployees().then(function(res){
            $scope.employees = res;
        });
    };
    $scope.homeWord;
    $scope.edit = function(index){
        $location.path("/edit/"+$scope.employees[index].name);
    };
}]);

app.controller("addCtrl",["$scope", "employeeService","statesService", function($scope,employeeService,statesService){
    $scope.states = statesService.states;
    $scope.newEmployee = {
        name: '',
        age: 0,
        address: {
            city: '',
            state: ''
        }
    };
    $scope.addEmployee = function(){
        employeeService.postEmployees($scope.newEmployee).then(function(res){
            alert(console.log(res));
            employeeService.getAllEmployees().then(function(res){
            $scope.employees = res;
            });
        });
    };
}]);

app.controller("editCtrl", ["$scope","employeeService","$location","$routeParams","statesService",function($scope,employeeService,$location,routeParams,statesService){
    $scope.states = statesService.states;
    employeeService.getAllEmployees($routeParams.name).then(function(res){
        $scope.employee = res;
    });
    $scope.submit = function(){
        employeeService.editEmployees($scope.employee).then(function(res){
            if(res.success){
                $location.path("/home")
            }else{
                $scope.err = res.message;
            }

        });
    };
}]);

app.filter("priceFilter", function(){
    return function(items, Min, Max){
        // console.log(Min,Max);
        min = Min || Number.MIN_VALUE;
        max = Max || Number.MAX_VALUE;
        // console.log(min,max);
        return items.filter(function(item){
            return item.age >= min  && item.age <= max ;
        });
    }
});

app.directive("meanSearch", function(){
    return{
        templateUrl: "template/mean-search.html",
        scope: { 
            meanWord: '=',// two way binding
            meanMax: "=",
            meanMin: "="
        },
    }
});

app.directive("edit", function(){
    return{
        templateUrl: "template/edit.html",
        scope: { 
            // meanWord: '=',// two way binding
            // meanMax: "=",
            // meanMin: "="
        },
    }
});

app.directive("ageValidator", function(){
    return {
        require: "ngModel",
        link: function(scope,elem,attr, ctrl){
            ctrl.$validators.ageValidator = function(modelValue,veiwValue){
                if(modelValue< 200 && modelValue >= 0){
                    return true;
                }
                return false
            }
        }
    };
});

app.config(["$routeProvider", function($routeProvider){
    $routeProvider.when("/home",{
        templateUrl: "template/home.html",
        controller: "homeCtrl"
    }).when("/add",{
        templateUrl: "template/add.html",
        controller: "addCtrl"
    }).when("/edit/:name",{
        templateUrl: "template/edit.html",
        controller: "editCtrl"
    }).otherwise({
        redirectTo: "/home"
    });
}]);

app.value("statesService",{
states: [
    {
        "name": "Alabama",
        "abbreviation": "AL"
    },
    {
        "name": "Alaska",
        "abbreviation": "AK"
    },
    {
        "name": "American Samoa",
        "abbreviation": "AS"
    },
    {
        "name": "Arizona",
        "abbreviation": "AZ"
    },
    {
        "name": "Arkansas",
        "abbreviation": "AR"
    },
    {
        "name": "California",
        "abbreviation": "CA"
    },
    {
        "name": "Colorado",
        "abbreviation": "CO"
    },
    {
        "name": "Connecticut",
        "abbreviation": "CT"
    },
    {
        "name": "Delaware",
        "abbreviation": "DE"
    },
    {
        "name": "District Of Columbia",
        "abbreviation": "DC"
    },
    {
        "name": "Federated States Of Micronesia",
        "abbreviation": "FM"
    },
    {
        "name": "Florida",
        "abbreviation": "FL"
    },
    {
        "name": "Georgia",
        "abbreviation": "GA"
    },
    {
        "name": "Guam",
        "abbreviation": "GU"
    },
    {
        "name": "Hawaii",
        "abbreviation": "HI"
    },
    {
        "name": "Idaho",
        "abbreviation": "ID"
    },
    {
        "name": "Illinois",
        "abbreviation": "IL"
    },
    {
        "name": "Indiana",
        "abbreviation": "IN"
    },
    {
        "name": "Iowa",
        "abbreviation": "IA"
    },
    {
        "name": "Kansas",
        "abbreviation": "KS"
    },
    {
        "name": "Kentucky",
        "abbreviation": "KY"
    },
    {
        "name": "Louisiana",
        "abbreviation": "LA"
    },
    {
        "name": "Maine",
        "abbreviation": "ME"
    },
    {
        "name": "Marshall Islands",
        "abbreviation": "MH"
    },
    {
        "name": "Maryland",
        "abbreviation": "MD"
    },
    {
        "name": "Massachusetts",
        "abbreviation": "MA"
    },
    {
        "name": "Michigan",
        "abbreviation": "MI"
    },
    {
        "name": "Minnesota",
        "abbreviation": "MN"
    },
    {
        "name": "Mississippi",
        "abbreviation": "MS"
    },
    {
        "name": "Missouri",
        "abbreviation": "MO"
    },
    {
        "name": "Montana",
        "abbreviation": "MT"
    },
    {
        "name": "Nebraska",
        "abbreviation": "NE"
    },
    {
        "name": "Nevada",
        "abbreviation": "NV"
    },
    {
        "name": "New Hampshire",
        "abbreviation": "NH"
    },
    {
        "name": "New Jersey",
        "abbreviation": "NJ"
    },
    {
        "name": "New Mexico",
        "abbreviation": "NM"
    },
    {
        "name": "New York",
        "abbreviation": "NY"
    },
    {
        "name": "North Carolina",
        "abbreviation": "NC"
    },
    {
        "name": "North Dakota",
        "abbreviation": "ND"
    },
    {
        "name": "Northern Mariana Islands",
        "abbreviation": "MP"
    },
    {
        "name": "Ohio",
        "abbreviation": "OH"
    },
    {
        "name": "Oklahoma",
        "abbreviation": "OK"
    },
    {
        "name": "Oregon",
        "abbreviation": "OR"
    },
    {
        "name": "Palau",
        "abbreviation": "PW"
    },
    {
        "name": "Pennsylvania",
        "abbreviation": "PA"
    },
    {
        "name": "Puerto Rico",
        "abbreviation": "PR"
    },
    {
        "name": "Rhode Island",
        "abbreviation": "RI"
    },
    {
        "name": "South Carolina",
        "abbreviation": "SC"
    },
    {
        "name": "South Dakota",
        "abbreviation": "SD"
    },
    {
        "name": "Tennessee",
        "abbreviation": "TN"
    },
    {
        "name": "Texas",
        "abbreviation": "TX"
    },
    {
        "name": "Utah",
        "abbreviation": "UT"
    },
    {
        "name": "Vermont",
        "abbreviation": "VT"
    },
    {
        "name": "Virgin Islands",
        "abbreviation": "VI"
    },
    {
        "name": "Virginia",
        "abbreviation": "VA"
    },
    {
        "name": "Washington",
        "abbreviation": "WA"
    },
    {
        "name": "West Virginia",
        "abbreviation": "WV"
    },
    {
        "name": "Wisconsin",
        "abbreviation": "WI"
    },
    {
        "name": "Wyoming",
        "abbreviation": "WY"
    }
]

});

app.factory("employeeService", ["$q","$http",employeeService]);