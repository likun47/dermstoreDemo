const express = require('express');
const bodyParser = require('body-parser');
const productCtrl = require('./controllers/product');
const userCtrl = require('./controllers/user');
const CategoryCtrl = require('./controllers/category');
const cors = require('cors');
const mongoose = require('mongoose');
const app = express();

// connect to Mongodb
mongoose.Promise = global.Promise;
// var promise = mongoose.connect("mongodb://likun47:mercury@ds155424.mlab.com:55424/mercury", {
//     useMongoClient: true
// });
var promise = mongoose.connect("mongodb://localhost:27017/mercury",{
   useMongoClient: true
});
promise.then(function() {
    console.log("Mongodb connected!");
});


app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(cors());


app.post('/auth/signup/',userCtrl.postUsers);
app.post('/auth/login/', userCtrl.getUser);
app.get('/api/product', productCtrl.getProducts);
app.get('/api/product/:id',productCtrl.getProduct);
app.get('/category', CategoryCtrl.getCategories);


// app.listen(process.env.port || 3000);
app.listen(3000);
console.log("my lord, you have the server");
