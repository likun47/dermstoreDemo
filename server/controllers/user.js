var User = require('../models/User');
var Product = require('../models/Product');

exports.postUsers = function(req,res,next){
    var user = new User(req.body);
    user.save(function(err){
        if(err){
            res.json();
        }else{
            console.log('insert success');
            res.send({flag:"true"});
        }
    });
}

// next point to next middleware, you can use next() to mannually invoke
exports.getUsers = function(req,res,next){
    User.find(function(err,docs){
        res.json(docs)
    })
}

exports.getUser = function(req,res){
    console.log(req.body);
    var user = new User(req.body);
    User.findOne({email: user.email},function(err,doc){
        if(err || doc == null || doc.password != user.password ){
            res.json({
                success:false
                // message: err.message
            })
        }else{
            res.json({
                success:true
                })
            }
        })
};


exports.putUser = function(req,res){
    var user = new User(req.body);
    User.findOneAndUpdate({name: user.name}, user, function(err){
        if(err){
            res.json({
                success:false,
                message: err.message
            })
        }else{
            res.json({
                success:true
            })
        }
    });
}

exports.deleteUser = function(req,res){
    var name=req.params.name;
    User.remove({name: name}, function(err,doc){
        if(err){
            console.log(err.message);
        }else{
            console.log('delete success');
        }
    });
}