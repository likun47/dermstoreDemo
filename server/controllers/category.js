var Category = require('../models/Category')

exports.postCategories = function(req,res,next){
    var product = new Category(req.body);
    product.save(function(err){
        if(err){
            res.json();
        }else{
            console.log('insert success');
        }
    });
}

// next point to next middleware, you can use next() to mannually invoke
exports.getCategories = function(req,res,next){
    Category.find(function(err,docs){
        res.json(docs)
    })
}

exports.getCategory = function(req,res){
    // req.params
    var idNumber = req.params.id;
    Category.findOne({id: idNumber},function(err,doc){
        res.json(doc)
    });
}

exports.putCategory = function(req,res){
    var product = new Category(req.body);
    Category.findOneAndUpdate({name: product.name}, product, function(err){
        if(err){
            res.json({
                success:false,
                message: err.message
            })
        }else{
            res.json({
                success:true
            })
        }
    });
}

exports.deleteCategory = function(req,res){
    var name=req.params.name;
    Category.remove({name: name}, function(err,doc){
        if(err){
            console.log(err.message);
        }else{
            console.log('delete success');
        }
    });
}