var Product = require('../models/Product')

exports.postProducts = function(req,res,next){
    var product = new Product(req.body);
    product.save(function(err){
        if(err){
            res.json();
        }else{
            console.log('insert success');
        }
    });
}

// next point to next middleware, you can use next() to mannually invoke
exports.getProducts = function(req,res,next){
    Product.find(function(err,docs){
        res.json(docs)
    })
}

exports.getProduct = function(req,res){
    // req.params
    var idNumber = req.params.id;
    Product.findOne({id: idNumber},function(err,doc){
        res.json(doc)
    });
}

exports.putProduct = function(req,res){
    var product = new Product(req.body);
    Product.findOneAndUpdate({name: product.name}, product, function(err){
        if(err){
            res.json({
                success:false,
                message: err.message
            })
        }else{
            res.json({
                success:true
            })
        }
    });
}

exports.deleteProduct = function(req,res){
    var name=req.params.name;
    Product.remove({name: name}, function(err,doc){
        if(err){
            console.log(err.message);
        }else{
            console.log('delete success');
        }
    });
}

