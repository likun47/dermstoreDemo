var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductSchema = new Schema({
    id: Number,
    image: String,
    title: String,
    volumn: String,
    description:String,
    price: String,
    quality: Number,
    name: String,
}, {collection: 'products'});

var Product = mongoose.model('Product',ProductSchema);

module.exports = Product;