var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CategorySchema = new Schema({
    id: Number,
    name: String,
}, {collection: 'categories'});

var Category = mongoose.model('Category',CategorySchema);

module.exports = Category;