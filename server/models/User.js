var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    email: {type: String, unique: true, require: true},
    password: String,
}, {collection: 'users'});

var User = mongoose.model('User',UserSchema);

module.exports = User;