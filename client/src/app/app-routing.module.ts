import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AppComponent} from './app.component';
import {UsersComponent} from './users/users.component';
import {BodyComponent} from './body/body.component';
import {AccountComponent} from './account/account.component';
import {SkinComponent} from './skin/skin.component';
import {SkinDetailComponent} from './skin/skin-detail/skin-detail.component';
import {CheckoutComponent} from './cart/checkout/checkout.component';
import {PlaceOrderComponent} from './cart/place-order/place-order.component';

const routes: Routes = [
  {
    path: 'home',
    component: BodyComponent
  },
  {
    path: 'users',
    component: UsersComponent
  },
  {
    path: 'account',
    component: AccountComponent
  },
  {
    path: 'products',
    component: SkinComponent
  },
  {
    path: 'product/:id',
    component: SkinDetailComponent
  },
  {
    path: 'checkout',
    component: CheckoutComponent
  },
  {
    path: 'place',
    component: PlaceOrderComponent
  },
  {
    path: '',
    component: BodyComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
