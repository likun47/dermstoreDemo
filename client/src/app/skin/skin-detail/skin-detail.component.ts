import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ProductService } from '../../op/shared/services/product.service';
import 'rxjs/add/operator/switchMap';
import {Product} from '../../shared/models/product';
import {CartService} from "../../op/shared/services/cart.service";


@Component({
  selector: 'app-product-detail',
  templateUrl: './skin-detail.component.html',
  styleUrls: ['./skin-detail.component.scss']
})
export class SkinDetailComponent implements OnInit {

  public product: Product;

  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private cartService: CartService
  ) { }

  ngOnInit() {
    this.route.paramMap
      .switchMap((params: ParamMap) => this.productService.getproduct(params.get('id')))
      .subscribe((res) => {
        this.product = res
      });
  }

  onAddToCart(product){
    this.cartService.addProductToCart(product);
  }

}
