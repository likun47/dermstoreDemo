import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ViewEncapsulation} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './body/header/header.component';
import { BodyComponent } from './body/body.component';
import { FooterComponent } from './body/footer/footer.component';
import { UsersComponent } from './users/users.component';
import { MobileMenuComponent } from './body/mobile-menu/mobile-menu.component';

import {AuthService} from './shared/auth/auth.service';
import { UserService} from './op/shared/services/user.service';
import { AccountComponent } from './account/account.component';
import { MyhoverDirective } from './body/header/myhover.directive';
import { SkinComponent } from './skin/skin.component';
import { SkinDetailComponent } from './skin/skin-detail/skin-detail.component';
import {ProductService} from './op/shared/services/product.service';
import { DataService} from './op/shared/services/data.service';
import { CartService} from './op/shared/services/cart.service';
import { CartComponent } from './cart/cart.component';
import { ProductThumbnailComponent } from './product-thumbnail/product-thumbnail.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { ShowcaseComponent } from './showcase/showcase.component';
import { SortFiltersComponent } from './sort-filters/sort-filters.component';
import { FiltersComponent } from './filters/filters.component';
import { CheckoutComponent } from './cart/checkout/checkout.component';
import { PlaceOrderComponent } from './cart/place-order/place-order.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BodyComponent,
    FooterComponent,
    UsersComponent,
    MobileMenuComponent,
    AccountComponent,
    MyhoverDirective,
    SkinComponent,
    SkinDetailComponent,
    CartComponent,
    ProductThumbnailComponent,
    SearchBarComponent,
    ShowcaseComponent,
    SortFiltersComponent,
    FiltersComponent,
    CheckoutComponent,
    PlaceOrderComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [
    AuthService,
    UserService,
    ProductService,
    DataService,
    CartService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
