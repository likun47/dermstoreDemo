import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Product } from '../../../shared/models/product';
import { APPCONFIG } from '../../../shared/config/app-config';

@Injectable()
export class ProductService {

  private productURL = APPCONFIG.APP_ENDPOINT + 'api/product';

  constructor(
    private http: Http
  ) { }

  getproducts(): Observable<Product[]> {
    console.log(this.productURL);
    return this.http.get(this.productURL)
      .map((res) => res.json());
  }

  getproduct(id: string): Observable<Product> {
    return this.http.get(this.productURL + '/' + id)
      .map((res) => res.json());
  }

}
