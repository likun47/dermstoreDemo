import {EventEmitter, Injectable} from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { User } from '../../../shared/models/user';
import { APPCONFIG } from '../../../shared/config/app-config';

@Injectable()
export class UserService {

  private userURL = APPCONFIG.APP_ENDPOINT + 'api/users/';
  public user: User;
  public change: EventEmitter<number>;

  constructor(
    private http: Http
  ) {
    this.getUser().subscribe(user => this.user = user);
    this.change = new EventEmitter<number>();
  }

  getUser(): Observable<any> {
    if (localStorage.getItem('msiopUser')) {
      return this.http.get(this.userURL + localStorage.getItem('msiopUser'))
        .map(res => {
          if (res.json() && res.json().success) {
            return res.json().user;
          }
        });
    } else {
      return Observable.of(null);
    }
  }

}
