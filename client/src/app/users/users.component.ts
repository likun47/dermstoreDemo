import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from './../shared/auth/auth.service';
import {User} from './../shared/models/user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class UsersComponent implements OnInit {

  registerForm: FormGroup;
  loginForm: FormGroup;
  errorMsg1: string;
  errorMsg2: String;
  user: User;

  constructor(private fb: FormBuilder,
              private router: Router,
              private authService: AuthService) {
    this.loginForm = this.fb.group({
      email: '',
      password: ''
    }),
      this.registerForm = this.fb.group({
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.minLength(6)]],
      });
  }
  ngOnInit(){

  }

  onSubmit1() {
    console.log(this.loginForm.value);
    this.authService.login(this.loginForm.value)
      .subscribe(result => {
        console.log(result.success);
        if (result.success === true) {
          this.router.navigate(['account']);
          localStorage.setItem("checkLogin",'true');
        } else {
          this.errorMsg1 = "You screw up the login";
          setTimeout(()=>{
            this.errorMsg1 = null;
          },1000)
        }
      });
  }

  onSubmit2() {
    this.user = this.registerForm.value;
    console.log(this.registerForm.value);
    if (!this.registerForm.valid) { // validation
      if (this.registerForm.get('email').errors) {
        this.errorMsg2 = 'Email is not valid.';
        setTimeout(()=>{
          this.errorMsg2 = null;
        },1000)
        return;
      }
      if (this.registerForm.get('password').errors) {
        this.errorMsg2 = 'Password should be at least 6 characters.';
        setTimeout(()=>{
          this.errorMsg2 = null;
        },1000)
        return;
      }
    }
    this.authService.signup(this.user)
      .subscribe(result => {
        if(result.flag == "true"){
          this.router.navigate(['account']);
          localStorage.setItem("checkLogin",'true');
        }
      })
  }
}


