import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import {UserService} from "../../op/shared/services/user.service";


@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {

  products: any;
  cartTotal: any;
  tax: any;

  constructor(
    private userService: UserService
  ) {
  }

  ngOnInit() {
    this.products = JSON.parse(localStorage.getItem('products'))
    this.cartTotal = JSON.parse(localStorage.getItem('carttotal'))
    this.tax = this.cartTotal * 0.1;
  }

  addShipcost(){
    return this.cartTotal += 6.99;
  }

  onSubmit(){
    this.userService.change.emit(1);
  }
}
