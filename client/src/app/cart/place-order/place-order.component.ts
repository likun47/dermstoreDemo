import {Component, DoCheck, OnInit} from '@angular/core';

@Component({
  selector: 'app-place-order',
  templateUrl: './place-order.component.html',
  styleUrls: ['./place-order.component.scss']
})
export class PlaceOrderComponent implements OnInit {

  order: Boolean = false;
  constructor(
  ) {}

  ngOnInit() {
    setTimeout(() => {
      this.order = true;
    },8000); }

}
