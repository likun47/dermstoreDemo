export class Product{
  id: number;
  image: string;
  title: string;
  volumn: string;
  description: string;
  price: string;
  quality: number;
  name: string
}
