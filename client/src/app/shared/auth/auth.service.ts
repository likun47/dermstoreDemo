import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { UserService } from '../../op/shared/services/user.service';
import { APPCONFIG } from '../config/app-config';

@Injectable()
export class AuthService {

  private authURI = APPCONFIG.APP_ENDPOINT + 'auth/';
  public token: string = null;
  public _id: string = null;
  public isAdmin: boolean = null;

  constructor(
    private http: Http,
    private userService: UserService
  ) {
    this.token = localStorage.getItem('dermToken');
    this._id = localStorage.getItem('dermUser');
    if (this._id) {
      this.userService.getUser()
        .subscribe((user) => this.isAdmin = user.active === 2);
    }
  }

  login(user: {email: string, password: string}): Observable<any> {
    return  this.http.post(this.authURI + 'login', user)
      .map((res: Response) => res.json());
  }

  signup(user: {email: string, password: string}): Observable<any> {
    return  this.http.post(this.authURI + 'signup', user)
      .map(res => res.json());
  }

  isLoggedIn(): boolean {
    return this.token !== null;
  }

  getToken() {
    return this.token;
  }

  logout() {
    this.token = null;
    localStorage.removeItem('dermToken');
    localStorage.removeItem('dermUser');
  }

}
