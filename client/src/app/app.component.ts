import {Component, OnInit, ViewEncapsulation} from '@angular/core';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit{
  title = 'app';
  firework = false;
  constructor(){
  }

  ngOnInit() {
    this.firework = true;
    setTimeout(()=> {
      this.firework = false;
    },3000);
  }

}

