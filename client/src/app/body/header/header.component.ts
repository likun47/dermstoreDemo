import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CartService} from '../../op/shared/services/cart.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit {

  checkLogin: any;

  constructor(private cartService: CartService,
              private router: Router) {

  }

  ngDoCheck() {
    this.checkLogin = localStorage.getItem('checkLogin');
    }

  ngOnInit() {
    console.log(this.checkLogin);
  }

  signOut() {
    localStorage.removeItem('checkLogin');
    this.router.navigate(['/home']);
  }

  onAddToCart(product){
    this.cartService.addProductToCart(product);
  }
}
