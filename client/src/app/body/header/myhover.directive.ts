import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[appMyhover]',
  host: {
    '(mouseenter)': 'onMouseEnter()',
    '(mouseleave)': 'onMouseLeave()'
  }
})
export class MyhoverDirective {

  private el: HTMLElement;

  constructor(el: ElementRef) { this.el = el.nativeElement; }

  onMouseEnter() {
    this.el.className = this.el.className + ' hover';
  }
  onMouseLeave() {
    this.el.className = this.el.className.replace(' hover', '');
  }

}
