const express = require('express');
const bodyParse = require('body-parser');
const cors = require('cors');

const app = express();

app.use(cors());
app.use(bodyParse.json());
app.use(bodyParse.urlencoded({extended:true}));

const USERS = [
  { email:"likun1@gmail.com", password:"lk123456"},
  { email:"likun2@gmail.com", password:"lk123456"},
  { email:"likun3@gmail.com", password:"lk123456"},
  { email:"likun4@gmail.com", password:"lk123456"},
  { email:"likun5@gmail.com", password:"lk123456"}
];

const PRODUCTS = [
  { id: 1,
    image:"http://localhost:3001/dasheng1.jpg",
    title: "SkinCeuticals1",
    volumn: "C E Ferulic(11 oz)",
    description: "A daytime, " +
    "vitamin C serum that provides environmental protection," +
    " lightens lines, firms skin and brightens your complexion.",
    price: "$265.00",
    quantity: 1,
    name: "abc"
  },
  { id: 2,
    image:"http://localhost:3001/dasheng2.jpg",
    title: "SkinCeuticals2",
    volumn: "ED Ferulic(13 oz)",
    description: "A daytime, " +
    "vitamin C serum that provides environmental protection," +
    " lightens lines, firms skin and brightens your complexion.",
    price: "$365.00",
    quantity: 1,
    name: "bcd"
  },
  { id: 3,
    image:"http://localhost:3001/dasheng3.jpg",
    title: "SkinCeuticals3",
    volumn: "C E Ferulic(4 oz)",
    description: "A daytime, " +
    "vitamin C serum that provides environmental protection," +
    " lightens lines, firms skin and brightens your complexion.",
    price: "$465.00",
    quantity: 1,
    name: "def"
  },
  { id: 4,
    image:"http://localhost:3001/dasheng4.jpg",
    title: "SkinCeuticals4",
    volumn: "C E Ferulic(1 oz)",
    description: "A daytime, " +
    "vitamin C serum that provides environmental protection," +
    " lightens lines, firms skin and brightens your complexion.",
    price: "$16.00",
    quantity: 1,
    name: "efg"
  },
  { id: 5,
    image:"http://localhost:3001/dasheng5.jpg",
    title: "SkinCeuticals",
    volumn: "C E Ferulic(123 oz)",
    description: "A daytime, " +
    "vitamin C serum that provides environmental protection," +
    " lightens lines, firms skin and brightens your complexion.",
    price: "$1235.00",
    quantity: 1,
    name: "ghi"
  }
]

app.post('/auth/signup/',(req,res)=>{
  USERS.forEach(each=>{
    if(each.email==req.body.email){
      res.json({res: 'duplicated email!'})
    }
  })
  res.json({res: 'sucess!'})
})

app.post('/auth/login/', (req,res)=>{
  USERS.forEach(each=>{
    if(each.email == req.body.email && each.password == req.body.password){
        res.json({boo: 'true'})
    }
  })
  res.json({boo:'false'})
})

app.get('/api/product',(req,res)=>{
  res.json(PRODUCTS);
  console.log('WE ARE HERE');
})

app.get('/api/product/:id', (req,res)=>{
  console.log('i am here');
  PRODUCTS.forEach(each=>{
    if(each.id == req.params.id){
        res.json(each)
    }
  })
})


app.listen(3000);
console.log("my lord, you have the server");
